import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Header } from 'react-native-elements';

export default class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userData: props.navigation.getParam('userData')
    }
  }

  render() {
    const { userData } = this.state;
    return [
      <Header
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'USER INFORMATION', style: { fontSize: 15, color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff' }}
      />,
      <View style={styles.container}>
        {userData.photoURL ? <Image
          style={{ width: 150, height: 150 }}
          source={{ uri: userData.providerData[0].photoURL }}
        /> : null}
        <Text style={styles.welcome}>Name:{userData.displayName}</Text>
        <Text style={styles.welcome}>Email :{userData.email}</Text>
      </View>
    ];
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
