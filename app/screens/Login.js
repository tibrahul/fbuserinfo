import React, { Component } from 'react';
import { AsyncStorage, ImageBackground, Dimensions, StyleSheet, TouchableOpacity, Image, View, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as UserAction from '../redux/actions/UserAction';

class Login extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this._checkUserInfo();
  }

  facebookLogin = async () => {
    this.props.UserAction.login();
  }

  _checkUserInfo = async () => {
    try {
      let data = await AsyncStorage.getItem('userInfo');
      if (data) {
        this.props.navigation.navigate("Dashboard", {
          userData: JSON.parse(data)
        });
      }
    } catch (error) {
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.loginResponse && nextProps.loginResponse.userInfo) {
      this.props.navigation.navigate("Dashboard", {
        userData: nextProps.loginResponse.userInfo
      });
    }
  }

  render() {
    return (
      <ImageBackground source={require('../assets/login.jpg')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          <Text style={{ fontSize: 30, fontWeight: 'bold' }}>Welcome To</Text>
          <TouchableOpacity onPress={() => this.facebookLogin()}>
            <Image style={{ width: Dimensions.get('window').width - 40, height: 50 }}
              source={require('../assets/fb_connect.png')}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginResponse: state.UserReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    UserAction: bindActionCreators(UserAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

