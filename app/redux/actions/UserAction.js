import * as types from './ActionTypes';
import { AsyncStorage } from 'react-native';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase'

export const login = () => {
    return async dispatch => {
        const result = await LoginManager.logInWithReadPermissions(['public_profile', 'user_friends', 'email']);
        if (result.isCancelled) {
            dispatch({ type: types.LOGIN_REJECTED, updatePayload: 'User cancelled request' });
        }
        const data = await AccessToken.getCurrentAccessToken();
        if (!data) {
            dispatch({ type: types.LOGIN_REJECTED, updatePayload: 'Something went wrong obtaining the users access token' });
        }
        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
        const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
        await this._storeUserInfo(firebaseUserCredential.user.toJSON(), dispatch);
        dispatch({ type: types.LOGIN_SUCCEEDED, updatePayload: firebaseUserCredential.user.toJSON() });
    }
}

_storeUserInfo = async (userInfo, dispatch) => {
    try {
        await AsyncStorage.setItem('userInfo', JSON.stringify(userInfo));
        this.setState({
            userInfo: JSON.parse(userInfo)
        })
        return true;
    } catch (error) {
        return false;       
    }
}