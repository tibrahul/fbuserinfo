import * as types from '../actions/ActionTypes';

export default (state = [], action) => {
    switch (action.type) {
        case types.LOGIN_SUCCEEDED:
            return {
                ...state,
                userInfo: action.updatePayload
            };
        case types.LOGIN_REJECTED:
            return {
                ...state,
                userInfoRej: action.updatePayload
            };
        default:
            return state;
    }
};
