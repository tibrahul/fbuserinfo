import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './app/redux/store/configure-store';
import Navigator from './navigation';

const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}